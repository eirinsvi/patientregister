package no.ntnu.eirinsvi.idatt2001.mappe2;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PatientRegisterTest {

    Patient testPatient1 = new Patient("123456789", "Snurre", "Bass", "sick", "Dr.Johnsen");

    // set socialsecurity number with more than 11 characters
    @Nested
    class addPatientMethodWorksWithExistingAndNonExistingPatients {
        @Test
        //@DisplayName("add patient who does not exist in register");
        void addPatientWhoDoesNotExistInRegister() {
            PatientRegister.addPatient(testPatient1);
            assertEquals(testPatient1, PatientRegister.getPatients().get(0));
        }

        @Test
        void addExistingPatientInRegister() {
            PatientRegister.addPatient(testPatient1);
            assertThrows(IllegalArgumentException.class, () -> PatientRegister.addPatient(testPatient1));
        }
    }
}
