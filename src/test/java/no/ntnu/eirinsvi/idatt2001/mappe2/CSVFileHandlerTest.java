package no.ntnu.eirinsvi.idatt2001.mappe2;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CSVFileHandlerTest {
    Patient testPatient1 = new Patient("123456789", "Snurre", "Bass", "sick", "Dr.Johnsen");

    @Test
    void writeAndReadToFile() throws IOException {
        //Writes to file
        File file = new File("src\\main\\resources\\SavedFiles\\testing.csv");
        PatientRegister.addPatient(testPatient1);
        CSVFileHandler.getFileHandler().exportToCSVFile(file, PatientRegister.getPatients());
        PatientRegister.getPatients().clear();

        //import the file
        CSVFileHandler.getFileHandler().importFromCSVFile(file, PatientRegister.getPatients());
        int numberOfPatients = 0;
        for(Patient patient : PatientRegister.getPatients()){
            numberOfPatients++;
        }
        assertEquals(1, numberOfPatients);
    }
}
