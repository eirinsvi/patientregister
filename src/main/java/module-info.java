module no.ntnu.eirinsvi.idatt2001.mappe2 {
    requires javafx.controls;
    requires javafx.fxml;

    opens no.ntnu.eirinsvi.idatt2001.mappe2 to javafx.fxml;
    exports no.ntnu.eirinsvi.idatt2001.mappe2;
}