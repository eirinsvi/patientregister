package no.ntnu.eirinsvi.idatt2001.mappe2;

import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

/**
 * class for factoring nodes for fxml
 * I used scenebuilder so this class is not used
 */
public class Factory {
    public static Node create(String nodeName) {
        if (nodeName.isBlank()) {
            return null;
        }  else if(nodeName.equalsIgnoreCase("borderpane")){
            return new BorderPane();
        } else if(nodeName.equalsIgnoreCase("vbox")){
            return new VBox();
        } else if(nodeName.equalsIgnoreCase("tableview")){
            return new TableView<>();
        } else if(nodeName.equalsIgnoreCase("menubar")){
            return new MenuBar();
        } else if(nodeName.equalsIgnoreCase("toolbar")){
            return new ToolBar();
        } else if(nodeName.equalsIgnoreCase("button")){
            return new Button();
        } else if(nodeName.equalsIgnoreCase("textfield")){
            return new TextField();
        } else if(nodeName.equalsIgnoreCase("label")){
            return new Label();
        }
        return null;
    }

}
