package no.ntnu.eirinsvi.idatt2001.mappe2;

import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.stage.Window;

import java.io.IOException;

public class AddNewPatientController {

    @FXML TextField firstName;
    @FXML TextField lastName;
    @FXML TextField socialSecurityNumber;
    @FXML TextField diagnosis;
    @FXML TextField generalPractitioner;


    public void initialize(){
        //Stage stage = (Stage) firstName.getSource.getScene().getWindow();
        //stage.setTitle("Add Patient");
        if(PatientInfo.getPatient() != null){
            firstName.setText(PatientInfo.getPatient().getFirstname());
            lastName.setText(PatientInfo.getPatient().getLastname());
            socialSecurityNumber.setText(PatientInfo.getPatient().getSocialSecurityNumber());
            diagnosis.setText(PatientInfo.getPatient().getDiagnosis());
            generalPractitioner.setText(PatientInfo.getPatient().getGeneralPractitioner());
        }
    }

    /**
     * Check if textFields firstname, lastname or socialSecurityNumber are blank
     */
    public boolean emptyTextFields(){
        if(diagnosis.getText().isBlank()){
            diagnosis.setText(" ");
        }
        if(generalPractitioner.getText().isBlank()){
            generalPractitioner.setText(" ");
        }
        if (firstName.getText().isBlank() || lastName.getText().isBlank()
                || socialSecurityNumber.getText().isBlank()){
            return true;
        } else{ return false; }
    }

    /**
     * Sends alert if textFields firstname, lastname or socialSecurityNumber are blank
     * adds patient to the register
     */
    @FXML
    private void clickedOk() throws IOException{
        if(emptyTextFields()){
            Alert equalFoundAlert = new Alert(Alert.AlertType.INFORMATION);
            equalFoundAlert.setHeaderText(null);
            equalFoundAlert.setTitle("Error");
            equalFoundAlert.setContentText("Fill in the fields for first name, last name and social secutiry number.");
            equalFoundAlert.showAndWait();
        } else {
            if(PatientInfo.getPatient() != null){
                PatientRegister.getPatients().remove(PatientInfo.getPatient());
            }
            Patient patient = new Patient(socialSecurityNumber.getText(), firstName.getText(), lastName.getText(), diagnosis.getText(), generalPractitioner.getText());
            try {
                PatientRegister.addPatient(patient);
            } catch (IllegalArgumentException e){
                Alert equalFoundAlert = new Alert(Alert.AlertType.INFORMATION);
                equalFoundAlert.setHeaderText(null);
                equalFoundAlert.setTitle("Error");
                equalFoundAlert.setContentText(e.getMessage());
                equalFoundAlert.showAndWait();
            }

            App.setRoot("main");
        }
    }
    @FXML
    private void clickedCancel() throws IOException{
        App.setRoot("main");
    }
}
