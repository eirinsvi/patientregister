package no.ntnu.eirinsvi.idatt2001.mappe2;

import java.io.IOException;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * Controller class connected to the mainpage
 */
public class MainController {

    //The tableview columns
    @FXML private TableView<Patient> tableView = new TableView<>();
    @FXML private TableColumn<Patient, String> socialSecurityNumberColumn;
    @FXML private TableColumn<Patient,String> firstnameColumn;
    @FXML private TableColumn<Patient,String> lastnameColumn;
    @FXML private TableColumn<Patient,String> diagnosis;
    @FXML private TableColumn<Patient,String> generalPractitioner;

    public void initialize(){
        setTableView();
    }

    /**
     * Method that sends you to the window for adding a patient
     */
    @FXML
    private void clickedAddPatientButton(ActionEvent event) throws IOException {
        PatientInfo.clearPatient();
        App.setRoot("addNewPatient");
        tableView.refresh();
    }

    /**
     * Method that sends you to the window for editing a patient
     * @param event
     */
    @FXML
    private void clickedEditPatientButton(ActionEvent event) throws IOException {
        if(tableView.getSelectionModel().getSelectedItem() != null) {
            PatientInfo.setPatient(tableView.getSelectionModel().getSelectedItem());
            App.setRoot("addNewPatient");
        } else {
            selectPatientAlert();
        }
        tableView.refresh();
    }

    /**
     * Method for deleting a patient
     * Shows a confirmation alert
     */
    @FXML
    private void clickedDeletePatientButton(){
        if(tableView.getSelectionModel().getSelectedItem() != null) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Delete patient");
            alert.setContentText("Are you sure you want to delete the patient?");
            ButtonType yesButton = new ButtonType("Yes", ButtonBar.ButtonData.YES);
            ButtonType cancelButton = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
            alert.getButtonTypes().setAll(yesButton, cancelButton);
            alert.showAndWait().ifPresent(type -> {
                if (type == yesButton) {
                    PatientRegister.getPatients().remove(tableView.getSelectionModel().getSelectedItem());
                }
            });
        } else {
            selectPatientAlert();
        }
    }

    /**
     * Method to import a file
     */
    @FXML
    private void clickedImportFromCSVMenuItem() throws IOException {
        CSVFileHandler.getFileHandler().importFromCSVFile(FilePicker.chooseExistingFile(), PatientRegister.getPatients());
        tableView.refresh();
    }

    /**
     * Method to save to a file
     */
    @FXML
    private void clickedExportToCSVMenuItem() throws IOException {
        FilePicker.newFile();
    }

    @FXML
    private void clickedExit(){
        Platform.exit();
    }

    /**
     * Method to display information about the application
     * Shows an information alert
     */
    @FXML
    private void clickedAbout(){
        Alert equalFoundAlert = new Alert(Alert.AlertType.INFORMATION);
        equalFoundAlert.setTitle("Information Dialog - About");
        equalFoundAlert.setHeaderText("Patients Register\nv0.1-SNAPSHOT");
        equalFoundAlert.setContentText("Application created by\nEirin Svinsås\n2021-05-02");
        equalFoundAlert.showAndWait();
    }

    /**
     * Method that display an information alert when you have not selected a patient
     */
    private void selectPatientAlert(){
        Alert equalFoundAlert = new Alert(Alert.AlertType.INFORMATION);
        equalFoundAlert.setTitle("Error");
        equalFoundAlert.setContentText("Please select a patient from the table");
        equalFoundAlert.showAndWait();
    }

    private void setTableView(){
        firstnameColumn.setCellValueFactory(new PropertyValueFactory<>("firstname"));
        lastnameColumn.setCellValueFactory(new PropertyValueFactory<>("lastname"));
        socialSecurityNumberColumn.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));
        diagnosis.setCellValueFactory(new PropertyValueFactory<>("diagnosis"));
        generalPractitioner.setCellValueFactory(new PropertyValueFactory<>("generalPractitioner"));
        tableView.setItems(PatientRegister.getPatients());
    }
}
