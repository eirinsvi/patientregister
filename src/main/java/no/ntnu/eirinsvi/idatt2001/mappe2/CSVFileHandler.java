package no.ntnu.eirinsvi.idatt2001.mappe2;

import javafx.collections.ObservableList;
import no.ntnu.eirinsvi.idatt2001.mappe2.Patient;
import no.ntnu.eirinsvi.idatt2001.mappe2.PatientRegister;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class CSVFileHandler {
    private static final CSVFileHandler fileHandler = new CSVFileHandler();

    public static CSVFileHandler getFileHandler(){
        return fileHandler;
    }
    /**
     * method that reads csv files and adds the patient to the patientregister
     * @param file file to read
     * @param register the patientregister
     * @throws IOException
     */
    public static void importFromCSVFile(File file, ObservableList<Patient> register) throws IOException{
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8));
        register.clear();
        String textLine;
        bufferedReader.readLine();
        while ((textLine = bufferedReader.readLine()) != null) {
            String[] patient = textLine.split(";");
            Patient p;
            if(patient.length<5){
                p = new Patient(patient[3], patient[0], patient[1], " ", patient[2]);
            } else{
                p = new Patient(patient[3], patient[0], patient[1], patient[4], patient[2]);
            }
            register.add(p);
        }
    }

    /**
     * method that fills the file with information from the patient register
     * @param file the file filled with information
     * @param register the patientregister
     * @throws IOException
     */
    public static void exportToCSVFile(File file, ObservableList<Patient> register) throws IOException{
        BufferedWriter fileWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file),StandardCharsets.UTF_8));
        fileWriter.write("firstName;lastName;generalPractitioner;socialSecurityNumber\n");

        register.forEach(s -> {
            try {
                fileWriter.write(s.getFirstname()+";"+s.getLastname()+";"+s.getGeneralPractitioner()+";"
                        +s.getSocialSecurityNumber()+";"+s.getDiagnosis()+"\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        fileWriter.close();

    }
}



