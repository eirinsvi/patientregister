package no.ntnu.eirinsvi.idatt2001.mappe2;

import javafx.scene.control.Alert;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;

public class FilePicker {
    private static final FileChooser fileChooser = new FileChooser();
    private static final Stage stage = new Stage();

    /**
     * Method that lets you choose a file and checks if it is a csv file
     * if it is not, it will throw an information alert saying you must choose
     * a csv file
     * @return File
     */
    public static File chooseExistingFile() {
        fileChooser.setTitle("Choose the file you want to use");
        fileChooser.setInitialDirectory(new File("src\\main\\resources\\SavedFiles"));
        File file = fileChooser.showOpenDialog(stage);
        if(file != null){
            if(file.getName().split("\\.")[1].equals("csv")){
                return file;
            } else {
                Alert equalFoundAlert = new Alert(Alert.AlertType.INFORMATION);
                equalFoundAlert.setTitle("Error");
                equalFoundAlert.setContentText("The chosen file type is not valid. Please choose another file by clicking on Select File or cancel the operation");
                equalFoundAlert.showAndWait();
            }
        }return null;

    }

    /**
     * Method that adds a new file
     * @throws IOException
     */
    public static void newFile() throws IOException {
        //If you choose an existing filename it automatically asks if you want to overwrite it, so I have not implemented that
        fileChooser.setInitialDirectory(new File("src\\main\\resources\\SavedFiles"));
        File file = fileChooser.showSaveDialog(null);
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("CSV","*.csv"));
        CSVFileHandler.getFileHandler().exportToCSVFile(file, PatientRegister.getPatients());
    }
}
