package no.ntnu.eirinsvi.idatt2001.mappe2;

/**
 * class that stores the patients information when entering a new scene
 */
public class PatientInfo {
    private static Patient patient= null;

    /**
     * Constructor hinders creation of objects of this class
     */
    private PatientInfo(){
        throw new IllegalStateException("This class cannot be instantiated");
    }

    /**
     * Method that can be used anywhere to get the current patient
     */
    public static Patient getPatient(){
        return patient;
    }

    /**
     * @param currentPatient is the patient selected from the tableview
     */
    public static void setPatient(Patient currentPatient){
        patient = currentPatient;
    }


    /**
     * When a patient is not selected anymore
     */
    public static void clearPatient(){
        patient = null;
    }
}
