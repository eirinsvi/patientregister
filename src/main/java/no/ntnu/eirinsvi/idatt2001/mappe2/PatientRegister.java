package no.ntnu.eirinsvi.idatt2001.mappe2;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.ArrayList;
/**
 * Class that keeps a register of all the patients
 */
public class PatientRegister {

    private final static ObservableList<Patient> patients = FXCollections.observableArrayList();

    /**
     * @return a list of all patients
     */
    public static ObservableList<Patient> getPatients() {
        return patients;
    }

    /**
     * method for adding a patient to the register.
     * @return a list of all patients
     * @throws IllegalArgumentException if patient already exists
     */
    public static void addPatient(Patient patient) throws IllegalArgumentException{
        if(!(patients.contains(patient))){
            patients.add(patient);
        } else {
            throw new IllegalArgumentException("Patient already exist");
        }
    }

}
