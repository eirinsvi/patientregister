package no.ntnu.eirinsvi.idatt2001.mappe2;

/**
 * class which handles one patient
 */

public class Patient {
    private String socialSecurityNumber;
    private String firstname;
    private String lastname;
    private String diagnosis;
    private String generalPractitioner;

    /**
     * A constructor that creates the patient
     * @param socialSecurityNumber the patients social security number,
     * supposed to be 11 characters but because some have 10 characters in patients.csv
     * the only limit is that it can not be more than 11 characters.
     * @param firstname the patients first name
     * @param lastname the patients last name
     * @param generalPractitioner the patients general practitioner
     * @param diagnosis - the patients diagnosis
     */
    public Patient(String socialSecurityNumber, String firstname, String lastname, String diagnosis, String generalPractitioner) {
        this.firstname = firstname;
        this.lastname = lastname;
        if(socialSecurityNumber.length()<12) {
            this.socialSecurityNumber = socialSecurityNumber;
        }
        this.diagnosis = diagnosis;
        this.generalPractitioner = generalPractitioner;
    }

    public Patient(String socialSecurityNumber, String firstname, String lastname, String generalPractitioner) {
        if(socialSecurityNumber.length()<12) { //eksempelet har nummer på bare 10 siffer
            this.socialSecurityNumber = socialSecurityNumber;
        }
        this.firstname = firstname;
        this.lastname = lastname;
        this.generalPractitioner = generalPractitioner;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "socialSecurityNumber='" + socialSecurityNumber + '\'' +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", diagnosis='" + diagnosis + '\'' +
                ", generalPractitioner='" + generalPractitioner + '\'' +
                '}';
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getGeneralPractitioner() {
        return generalPractitioner;
    }

    public void setGeneralPractitioner(String generalPractitioner) {
        this.generalPractitioner = generalPractitioner;
    }
}
